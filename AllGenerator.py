from Generator import Request_Response_ParserGenerator, CoordinatorGenerator, View_Delegate_ControllerGenerator, ModuleGenertor


def writeAllToFile():
    CoordinatorGenerator.writeToFile()
    Request_Response_ParserGenerator.writeToFile()
    View_Delegate_ControllerGenerator.writeToFile()
    ModuleGenertor.writeToFile()


if __name__ == '__main__':
    writeAllToFile()
