# Usage
If you want to use this tool, you need to done these blow steps.

1. `git clone`
2. Open the **terminal** and then `cd` to the folder

    This tool is based on python 2.7.
    
    And each MAC OS has python 2.7, so you can use this immediately.

3. Run command: `python AllGenerator.py`
4. Input the prefix and the author name
    
    The code will be generated at the same path you run the script.
    
5. Drag the file what you want to the mapp project.