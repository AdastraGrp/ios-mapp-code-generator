# -*- coding: UTF-8 -*-
import environment
import file_operator

changeTemplate = "{ChangeString}"

requestTemplate = environment.head_string + """
import Alamofire
import Foundation

enum """ + changeTemplate + """Request {
    /// Here to write the request kind you want to use
}

extension """ + changeTemplate + """Request: URLRequestConvertible {
    var baseURLString: String {
        return <#base#>
    }

    var method: HTTPMethod {
        return <#.get#>
    }
    
    var path: String? {
        // the path you need to get
        return <#path#>
    }
    
    // MARK: URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        <#Here need to conver a request to URLRequest#>

        return <#Request#>
    }
    
    
}
"""

responseTemplate = environment.head_string + """
import Foundation

struct """ + changeTemplate + """Response {
    <#Response model value#>
}
"""

parserTemplate = environment.head_string + """
import Foundation

struct """ + changeTemplate + """Parser: ParserType {
    typealias ParsedType = """ + changeTemplate + """Response
    
    func parse(networkResponse: NetworkResponse) -> """ + changeTemplate + """Response {
        <#function body#>
        
        return """ + changeTemplate + """Response(<#init parameter#>)
    }
}
"""

serviceTemplate = environment.head_string + """
import Foundation

struct """ + changeTemplate + """Service: NetworkService {
    typealias Output = """ + changeTemplate + """Response

    typealias Input = """ + changeTemplate + """Request

    typealias Parser = """ + changeTemplate + """Parser

    var dispatcher: NetworkDispatcher

    var parser: Parser
}
"""

serviceFacadeTemplate = environment.head_string + """
import Foundation

protocol """ + changeTemplate + """ServiceFacade {
    /// Outside need to get 
    func <#name#>(<#parameters#>,callback: @escaping((Response<<#""" + changeTemplate + """Response#>, Error>) -> Void))
    
}
"""

serviceFacadeImpTemplate = environment.head_string + """
import Foundation

struct """ + changeTemplate + """ServiceFacadeImp: """ + changeTemplate + """ServiceFacade {
    private let service: """ + changeTemplate + """Service

    init(service: """ + changeTemplate + """Service) {
        self.service = service
    }

    /// you can use service.perform to get the response data
    func <#name#>(<#parameters#>,callback: @escaping((Response<<#""" + changeTemplate + """Response#>, Error>) -> Void)) {

    }
}
"""

def writeRequestToFile():
    print "Prepare to generate Request file"
    content = requestTemplate.replace(changeTemplate, environment.module_string)
    file_operator.createFileWithContent(path=environment.basicPath + "/Request",
                                        fileName="{}Request.swift".format(environment.module_string),
                                        content=content)
def writeResponseToFile():
    print "Prepare to generate Response file"
    content = responseTemplate.replace(changeTemplate, environment.module_string)
    file_operator.createFileWithContent(path=environment.basicPath + "/Response",
                                        fileName="{}Response.swift".format(environment.module_string),
                                        content=content)

def writeParseToFile():
    print "Prepare to generate Parse file"
    content = parserTemplate.replace(changeTemplate, environment.module_string)
    file_operator.createFileWithContent(path=environment.basicPath + "/Parse",
                                        fileName="{}Parse.swift".format(environment.module_string),
                                        content=content)

def writeServiceToFile():
    print "Prepare to generate Serive file"
    content = serviceTemplate.replace(changeTemplate, environment.module_string)
    file_operator.createFileWithContent(path=environment.basicPath + "/Service",
                                        fileName="{}Service.swift".format(environment.module_string),
                                        content=content)

def writeServiceFacadeToFile():
    print "Prepare to generate ServiceFacade file"
    content = serviceFacadeTemplate.replace(changeTemplate, environment.module_string)
    file_operator.createFileWithContent(path=environment.basicPath + "/ServiceFacade",
                                        fileName="{}ServiceFacade.swift".format(environment.module_string),
                                        content=content)

def writeServiceFacadeImpToFile():
    print "Prepare to generate ServiceFacadeImp file"
    content = serviceFacadeImpTemplate.replace(changeTemplate, environment.module_string)
    file_operator.createFileWithContent(path=environment.basicPath + "/ServiceFacade/Imp",
                                        fileName="{}ServiceFacadeImp.swift".format(environment.module_string),
                                        content=content)


def writeToFile():
    writeParseToFile()
    writeRequestToFile()
    writeResponseToFile()
    writeServiceToFile()
    writeServiceFacadeToFile()
    writeServiceFacadeImpToFile()