# -*- coding: UTF-8 -*-
import time

head_string = """//
// This file is generate by Di.Wu's swift MAPP's architecture file generator
// Enjoy coding
//
// |FileName|
// mapp
//
// Created by {} on {}.
// Copyright © {} Home Credit Consumer Finance ltd. All rights reserved.
//
"""



module_string = "{}".format(raw_input("Please input the Class prefix:\n"))

now = int(time.time())

timeStruct = time.localtime(now)

head_string = head_string.format(raw_input("Please input the Author Name:\n"), time.strftime("%d/%m/%Y", timeStruct), time.strftime("%Y", timeStruct))

basicPath = module_string

controllerPath = "Controller"

coordinatorPath = "Coordinator"

fileNameTemplate = "|FileName|"




