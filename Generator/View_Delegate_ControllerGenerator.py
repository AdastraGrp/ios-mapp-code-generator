# -*- coding: UTF-8 -*-
import environment
import file_operator

changeTemplate = "{ChangeString}"

viewDelegateTemplate = environment.head_string + """
import Foundation

protocol """ + changeTemplate + """ViewOutputDelegate: class, ViewControllerAlertHandler {
    // TODO: -<#YOUR NAME#>- controller want coordinator to implement

    <#Protocol body#>
}
"""

viewTemplate = environment.head_string + """
import Foundation
/// Controller Protocol
protocol """ + changeTemplate + """View: BaseView {
    var delegate: """ + changeTemplate + """ViewOutputDelegate? { get set }

    // TODO: -<#YOUR NAME#>-
}
"""

controllerTemplate = environment.head_string + """
import UIKit
// MARK -<##>
final class """ + changeTemplate + """ViewController: BaseViewController,""" + changeTemplate + """View {
    weak var delegate: """ + changeTemplate + """ViewOutputDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        <#Here to write basic code#>
    }

}
"""

filePath = environment.basicPath + "/" + environment.controllerPath

def writeDelegateToFile():
    print "Prepare to generate ViewDelegate file"
    content = viewDelegateTemplate.replace(changeTemplate, environment.module_string)
    file_operator.createFileWithContent(path=filePath,
                                        fileName="{}ViewOutputDelegate.swift".format(environment.module_string),
                                        content=content)


def writeViewToFile():
    print "Prepare to generate View file"
    content = viewTemplate.replace(changeTemplate, environment.module_string)
    file_operator.createFileWithContent(path=filePath,
                                        fileName="{}View.swift".format(environment.module_string),
                                        content=content)


def writeControllerToFile():
    print "Prepare to generate Controller file"
    content = controllerTemplate.replace(changeTemplate, environment.module_string)
    file_operator.createFileWithContent(path=filePath,
                                        fileName="{}ViewController.swift".format(environment.module_string),
                                        content=content)


def writeToFile():
    print "开始生成Controller相关 file"
    writeControllerToFile()
    writeViewToFile()
    writeDelegateToFile()


if __name__ == '__main__':
    writeToFile()
