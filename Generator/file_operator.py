# -*- coding: UTF-8 -*-
import os
import environment


class fileInfo:
    def __init__(self, exist_file=False, is_folder=False):
        # type: (bool, bool) -> file_info
        self.exist_file = exist_file
        self.is_folder = is_folder

    exist_file = False
    is_folder = False


def fileExistAtPath(path):
    if os.path.exists(path):
        if os.path.isdir(path):
            return fileInfo(True, True)
        else:
            return fileInfo(True, False)
    return fileInfo()


def createFolder(path):
    if fileExistAtPath(path).exist_file:
        raise Exception("File exists at path:{}".format(path))
    os.makedirs(path)


def createFileWithContent(path, fileName, content):
    print "Prepare to create file:{} at the directory:{}".format(fileName, path)

    print environment.fileNameTemplate

    content = content.replace(environment.fileNameTemplate, fileName)

    exist = fileExistAtPath(path)
    if exist.exist_file:
        if exist.is_folder is False:
            raise Exception("File at path:{} is not a folder".format(path))
    else:
        createFolder(path)

    with open("{}/{}".format(path, fileName), "w") as f:
        f.write(content)

    print "File generated completed"


def write(baseTemplate, changeTemplate, changeModuleString, filePath, prepareWriteName):
    print "Prepare to generate {} file"
    content = baseTemplate.replace(changeTemplate, changeModuleString)
    createFileWithContent(path=filePath,
                          fileName="{}{}.swift".format(changeModuleString, prepareWriteName),
                          content=content)
