# coding=utf-8
import environment
import file_operator

changeTemplate = "{changeTemplate}"

moduleFactoryTemplate = environment.head_string + """
import Foundation

protocol """ + changeTemplate + """ModuleFactory {
    func make""" + changeTemplate + """ViewOutput() -> """ + changeTemplate + """View
}
"""

moduleFactoryExtensionTemplate = environment.head_string + """
import Foundation

extension ModuleFactoryImp: """ + changeTemplate + """ModuleFactory {
    func make""" + changeTemplate + """ViewOutput() -> """ + changeTemplate + """View {
        let viewController = """ + changeTemplate + """ViewController()

        return viewController
    }
}
"""

filePath = environment.basicPath + "/ModuleFactory"


def writeModuleFactoryToFile():
    file_operator.write(moduleFactoryTemplate, changeTemplate, environment.module_string,
                        filePath=filePath,
                        prepareWriteName="ModuleFactory")


def writeModuleFactoryExtensionToFile():
    file_operator.write(moduleFactoryExtensionTemplate, changeTemplate, environment.module_string,
                        filePath=filePath,
                        prepareWriteName="ModuleFactoryExtension")


def writeToFile():
    writeModuleFactoryToFile()
    writeModuleFactoryExtensionToFile()
