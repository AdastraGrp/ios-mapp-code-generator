# coding=utf-8
import environment
import file_operator

changeTemplate = "{ChangeString}"
coordinatorTemplate = environment.head_string + """
import Foundation

final class """ + changeTemplate + """Coordinator: BaseCoordinator, """ + changeTemplate + """CoordinatorOutput {
    weak var delegate: """ + changeTemplate + """CoordinatorOutputDelegate?
    /// factory: generate viewController
    <#private let factory: """ + changeTemplate + """ModuleFactory#>
    /// router: For Navigation 
    private let router: Router

    init(router: Router<#, factory: """ + changeTemplate + """ModuleFactory#>) {
        self.router = router
        self.factory = factory
        super.init()
    }

    override func start(with options: CoordinatorStartOptions) {
        run""" + changeTemplate + """()
    }
}

private extension """ + changeTemplate + """Coordinator {
    func run""" + changeTemplate + """() {
        // TODO -<#YOUR NAME#>- implement your code.
    }
}

extension """ + changeTemplate + """Coordinator: """ + changeTemplate + """ViewOutputDelegate, AlertCoordinator {
    // TODO -<#YOUR NAME#>- implement your Delegate.
}
"""

coordinatorOutputTemplate = environment.head_string + """
import Foundation

protocol """ + changeTemplate + """CoordinatorOutput: class {
    var delegate: """ + changeTemplate + """CoordinatorOutputDelegate? { get set }
}
"""

coordinatorOutputDelegateTemplate = environment.head_string + """
import Foundation

protocol """ + changeTemplate + """CoordinatorOutputDelegate: class {
    // TODO -<#YOUR NAME#>- write code that you need.
}
"""

filePath = environment.basicPath + "/" + environment.coordinatorPath

def writeCoordinatorToFile():
    print "Prepare to generate Coordinator file"
    content = coordinatorTemplate.replace(changeTemplate, environment.module_string)
    file_operator.createFileWithContent(path=filePath,
                                        fileName="{}Coordinator.swift".format(environment.module_string),
                                        content=content)


def writeCoordinatorOutputToFile():
    print "Prepare to generate CoordinatorOutput file"
    content = coordinatorOutputTemplate.replace(changeTemplate, environment.module_string)
    file_operator.createFileWithContent(path=filePath,
                                        fileName="{}CoordinatorOutput.swift".format(environment.module_string),
                                        content=content)


def writeCoordinatorOutputDelegateToFile():
    print "Prepare to generate CoordinatorOutputDelegate file"
    content = coordinatorOutputDelegateTemplate.replace(changeTemplate, environment.module_string)
    file_operator.createFileWithContent(path=filePath,
                                        fileName="{}CoordinatorOutputDelegate.swift".format(environment.module_string),
                                        content=content)

def writeToFile():
    print "Prepare to generate Coordinator files"
    writeCoordinatorToFile()
    writeCoordinatorOutputToFile()
    writeCoordinatorOutputDelegateToFile()

if __name__ == '__main__':
    writeToFile()